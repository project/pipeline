class CreatePipes < ActiveRecord::Migration[5.1]
  def change
    create_table :pipes do |t|
      t.string          :token, index: true
      t.string          :email
      t.text            :description
      t.string          :status
      t.string          :ticket
      t.string          :link
      t.text            :groups
      t.text            :success
      t.text            :timeframe
      t.date            :start
      t.integer         :urgency
      t.float           :budget
      t.text            :funding
      t.text            :additional_funding
      t.timestamps
    end
  end
end
