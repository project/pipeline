class AddTokenDigestToPipes < ActiveRecord::Migration[5.1]
  def change
    add_column :pipes, :token_digest, :string
  end
end
