class StaticController < ApplicationController
  def home
  	if logged_in?
      redirect_to backstage_url
    end
  end
end
