class UsersController < ApplicationController
  before_action :logged_in_user
  before_action :this_user

  def show
    @user = User.find(params[:id])
  end

  def edit
  end

  def update
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  private

    def user_params
      params.require(:user).permit(:username, :email, :password,
                                   :password_confirmation)
    end

    # Confirms the correct user.
    def this_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end

end
