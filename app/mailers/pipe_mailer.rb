class PipeMailer < ApplicationMailer
  default from: "mailer@pipeline.torproject.net"
  def sample_email
    mail(to: "hiro@torproject.org", subject: 'Sample Email')
  end

end
