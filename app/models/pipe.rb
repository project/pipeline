class Pipe < ApplicationRecord
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: Rails.configuration.valid_email_regex }

  before_create :generate_new_token

  # Generates a new token.
  def generate_new_token
    self.token = SecureRandom.urlsafe_base64
  end


  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |pipe|
        csv << pipe.attributes.values_at(*column_names)
      end
    end
  end



end
