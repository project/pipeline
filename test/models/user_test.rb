require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @admin = User.new(username: "hiro", email: "hiro@torproject.org", role: "admin",
                      password: "foobar43", password_confirmation: "foobar43")
    @user = User.new(username: "roger", email: "arma@torproject.org", role: "user",
                     password: "foobar43", password_confirmation: "foobar43")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "should not have valid email" do
    user = User.new(username: "roger", email: "arma@torproject", role: "user",
                    password: "foobar43", password_confirmation: "foobar43")
    assert_not user.valid?
  end

  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?('')
  end

end
