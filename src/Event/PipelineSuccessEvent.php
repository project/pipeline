<?php

declare(strict_types = 1);

namespace Drupal\pipeline\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\pipeline\Plugin\PipelinePipelineInterface;

/**
 * A successful pipeline completion event.
 */
class PipelineSuccessEvent extends Event {

  /**
   * The pipeline that triggered this event.
   *
   * @var \Drupal\pipeline\Plugin\PipelinePipelineInterface
   */
  protected $pipeline;

  /**
   * Constructs a PipelineSuccessEvent object.
   *
   * @param \Drupal\pipeline\Plugin\PipelinePipelineInterface $pipeline
   *   The pipeline.
   */
  public function __construct(PipelinePipelineInterface $pipeline) {
    $this->pipeline = $pipeline;
  }

  /**
   * Retrieves the pipeline that triggered this event.
   *
   * @return \Drupal\pipeline\Plugin\PipelinePipelineInterface
   *   The pipeline.
   */
  public function getPipeline(): PipelinePipelineInterface {
    return $this->pipeline;
  }

  /**
   * Sets the pipeline that triggered this event.
   *
   * @param \Drupal\pipeline\Plugin\PipelinePipelineInterface $pipeline
   *   The pipeline.
   */
  public function setPipeline(PipelinePipelineInterface $pipeline): void {
    $this->pipeline = $pipeline;
  }

}
