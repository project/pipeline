<?php

declare(strict_types = 1);

namespace Drupal\pipeline\Traits;

use Drupal\DrupalExtension\Context\RawDrupalContext;

/**
 * Behat step definitions for testing the pipeline.
 *
 * This code is shared between the (deprecated) PipelineSubContext class and the
 * (recommended) PipelineContext class.
 */
trait PipelineContextTrait {

  /**
   * Reset the pipeline orchestrator through the API.
   *
   * @param string $pipeline
   *   The pipeline ID.
   *
   * @Given I reset the :pipeline pipeline
   */
  public function iResetThePipelineOrchestrator($pipeline) {
    $this->visitPath("admin/content/pipeline/$pipeline/reset");
  }

}
