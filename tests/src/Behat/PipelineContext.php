<?php

declare(strict_types = 1);

namespace Drupal\Tests\pipeline\Behat;

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Drupal\pipeline\Traits\PipelineContextTrait;

/**
 * Behat step definitions for testing the pipeline.
 */
class PipelineContext extends RawDrupalContext {

  use PipelineContextTrait;

}
