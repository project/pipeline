<?php

/**
 * @file
 * Contains \PipelineSubContext.
 */

use Drupal\DrupalExtension\Context\DrupalSubContextBase;
use Drupal\pipeline\Traits\PipelineContextTrait;

/**
 * Behat step definitions for testing the pipeline.
 *
 * @deprecated
 *   Subcontexts are deprecated in Behat Drupal Extension 4.x. Instead use
 *   PipelineContext and include it directly in your behat.yml file.
 *
 * @see \Drupal\Tests\pipeline\Behat\PipelineContext
 * @see https://github.com/jhedstrom/drupalextension/issues/518
 */
class PipelineSubContext extends DrupalSubContextBase {

  use PipelineContextTrait;

}
