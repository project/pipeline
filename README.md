# README

Pipeline is a minimal web app to collect and manage fund-raising ideas.

The torgit repository is fetched with a custom git hook.
A repository with hooks you might need is provided at: ...

This app is hosted at: ...

More information can be found at the infrastructure wiki: ...

Pipeline runs Rails 5 and Ruby

Pipelien is JavaScript free.

An onion service is accesible at: ...

This app is deployed via: ...

This app is deployed to: ...

If you would like an account created, or for any other query, please get in
touch with hiro at tpo.
